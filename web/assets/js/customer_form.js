$( document ).ready(function() {
    var selected_country = $( "#customer_location_country" ).data( "selected" );
    var selected_region = "";
    var regions = [];
    var cities = [];
    var selected_city = "";
    var app7 = new Vue({
        el: '#app-7',
        watch: {
            'selected_country': function(val, oldVal){
                if (typeof val !== 'undefined' && val!="") {
                    app7.RegionsLoading = true;
                    var options = {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                        }
                    };
                    app7.regions = [];
                    this.$http.get('/api/v1/countries/'+val+'/regions', options).then (
                        function(response) {
                            app7.regions = response.body;
                            console.log ("Regions: ", app7.regions);
                            app7.RegionsLoading = false;
                        },
                        function (response){
                            console.log (response);
                            app7.RegionsLoading = false;
                        });
                }
            },
            'selected_region': function(val, oldVal){
                if (typeof val !== 'undefined' && val!="" && app7.no_load_cities==false) {
                    app7.CitiesLoading = true;
                    var options = {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                        }
                    };
                    app7.cities = [];
                    this.$http.get('/api/v1/regions/'+val, options).then (
                        function(response) {
                            app7.CitiesLoading = false;
                            var cities = response.body.cities;
                            console.log ("Cities: ", cities);
                            app7.cities = cities;
                        },
                        function (response){
                            app7.CitiesLoading = false;
                            console.log (response);
                        });
                }
            }
        },
        data: {
            CitiesLoading: false,
            RegionsLoading: false,
            //countries: [],
            selected_country: selected_country,
            cities: cities,
            selected_city: selected_city,
            regions: regions,
            selected_region: selected_region,
            loading:false,
            no_load_cities: false
        },
        methods: {
        }
    })
    if (typeof selected_country != "undefined" && selected_country!="") {
        $.getJSON( "../api/v1/countries/"+selected_country, function( data ) {
            app7.regions = data.regions;
            app7.no_load_cities = true;
            app7.selected_region = $("#customer_location_regionId").data("selected");
            if (app7.selected_region!="") {
                $.getJSON( "../api/v1/regions/"+app7.selected_region, function( data ) {
                    app7.cities = data.cities;
                    app7.selected_city = $("#customer_location_cityId").data("selected");
                    console.log ("customer_location_cityId: ", app7.selected_city);
                    app7.no_load_cities = false;
                });
            }else {
                app7.no_load_cities = false;
            }
        });
    }
});
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegionRepository")
 */
class Region
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $countryId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="region", cascade={"persist", "remove", "merge"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $cities;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="regions", fetch="EAGER")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    public $country;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     *
     * @return Region
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add city
     *
     * @return $this
     */
    public function addCity(City $city)
    {
        $city->setRegion($this); // synchronously updating inverse side
        $this->cities[] = $city;
        return $this;
    }

    /**
     * Get cities
     *
     * @return ArrayCollection
     */
    public function getCities (){
        return $this->cities;
    }

    /**
     * Add Country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get Country
     *
     * @return $this
     */
    public function setCountry (Country $country)
    {
        $this->country = $country;
        return $this;
    }
}


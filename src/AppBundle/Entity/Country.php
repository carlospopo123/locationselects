<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_2a", type="string", length=50, unique=true)
     */
    private $code2a;

    /**
     * @var string
     *
     * @ORM\Column(name="code_3a", type="string", length=50, nullable=true)
     */
    private $code3a;

    /**
     * @var string
     *
     * @ORM\Column(name="code_3n", type="string", length=50, nullable=true)
     */
    private $code3n;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Customer", mappedBy="country", cascade={"persist", "remove", "merge"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $Customers;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="country", cascade={"persist", "remove", "merge"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity="Region", mappedBy="country", cascade={"persist", "remove", "merge"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $regions;


    public function __construct() {
        $this->Customers = new ArrayCollection();
        $this->Cities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code2a
     *
     * @param string $code2a
     *
     * @return Country
     */
    public function setCode2a($code2a)
    {
        $this->code2a = $code2a;

        return $this;
    }

    /**
     * Get code2a
     *
     * @return string
     */
    public function getCode2a()
    {
        return $this->code2a;
    }

    /**
     * Set code3a
     *
     * @param string $code3a
     *
     * @return Country
     */
    public function setCode3a($code3a)
    {
        $this->code3a = $code3a;

        return $this;
    }

    /**
     * Get code3a
     *
     * @return string
     */
    public function getCode3a()
    {
        return $this->code3a;
    }

    /**
     * Set code3n
     *
     * @param string $code3n
     *
     * @return Country
     */
    public function setCode3n($code3n)
    {
        $this->code3n = $code3n;

        return $this;
    }

    /**
     * Get code3n
     *
     * @return string
     */
    public function getCode3n()
    {
        return $this->code3n;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add customer
     *
     * @return $this
     */
    public function addCustomer(Customer $Customer)
    {
        $Customer->addCountry($this); // synchronously updating inverse side
        $this->Customers[] = $Customer;
        return $this;
    }

    /**
     * Get customer
     *
     * @return ArrayCollection
     */
    public function GetCustomers (){
        return $this->Customers;
    }

    /**
     * Add city
     *
     * @return $this
     */
    public function addCity(City $city)
    {
        $city->setCountry($this); // synchronously updating inverse side
        $this->cities[] = $city;
        return $this;
    }

    /**
     * Get cities
     *
     * @return ArrayCollection
     */
    public function getCities (){
        return $this->cities;
    }

    /**
     * Add region
     *
     * @return $this
     */
    public function addRegion(Region $region)
    {
        $region->setCountry($this); // synchronously updating inverse side
        $this->regions[] = $region;
        return $this;
    }

    /**
     * Get regions
     *
     * @return ArrayCollection
     */
    public function getRegions (){
        return $this->regions;
    }
}


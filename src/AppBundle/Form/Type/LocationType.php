<?php
namespace AppBundle\Form\Type;

use AppBundle\Repository\CityRepository;
use AppBundle\Repository\CountryRepository;
use AppBundle\Repository\RegionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\FormInterface;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $country_field = array(
            'required'    => false,
            'placeholder' => 'Choose country',
            'empty_data'  => null,
            'mapped' => true,
            'attr' => array('v-model' => 'selected_country')
        );
        $builder->add('country', CountryType::class, $country_field);
        $region_field = array(
            'choices' => array('loading...'=>'', '{{region.name}}'=>''),
            'label' => 'Region',
            'required'    => false,
            'placeholder' => 'Choose region',
            'empty_data'  => null,
            'mapped' => true,
            'attr' => array('v-model' => 'selected_region'),
            'choice_attr' => [
                '{{region.name}}' => [
                    'v-for' => 'region in regions',
                    'v-bind:value' => 'region.id'
                ],
                'loading...' => [
                    'v-if' => 'RegionsLoading',
                ]
            ]
        );
        $builder->add('regionId', ChoiceType::class, $region_field);
        /*$builder->add('cityId', EntityType::class, array(
            'class' => 'AppBundle:City',
            'query_builder' => function (CityRepository $city_repository) {
                return $city_repository->createQueryBuilder('c')
                    ->where('c.countryId = :first_country')
                    // match id of joined `CursoAcademico`
                    ->setParameter('first_country', 2)
                    ->setMaxResults(10);
            },
            'choice_label' => 'name',
            'label' => 'City',
            'required'    => false,
            'placeholder' => 'Choose city',
            'empty_data'  => null,
            'mapped' => true,
            'attr' => array('v-model' => 'cities')
        ));*/
        $city_field = array(
            //'class' => 'AppBundle:City',
            'choices' => array('loading...'=>'', '{{city.name}}'=>''),
            'label' => 'City',
            'required'    => false,
            'placeholder' => 'Choose city',
            'empty_data'  => null,
            'mapped' => true,
            'attr' => array('v-model' => 'selected_city'),
            'choice_attr' => [
                '{{city.name}}' => [
                    'v-for' => 'city in cities',
                    'v-bind:value' => 'city.id'
                ],
                'loading...' => [
                    'v-if' => 'CitiesLoading',
                ]
            ]
        );
        $builder->add('cityId', ChoiceType::class, $city_field);
        /*$builder->get('sport')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $sport = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $sport);
            }
        );*/

        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use($city_field, $region_field, $country_field){
                $form = $event->getForm();
                $data = $event->getForm()->getParent()->getData();
                $filtered_city_field = $city_field;
                $filtered_region_field = $region_field;
                $filtered_country_field = $country_field;

                $filtered_city_field['attr']['data-selected'] = $data->getCityId();
                $filtered_region_field['attr']['data-selected'] = $data->getRegionId();
                if (!empty($data->getCountry())) {
                    $filtered_country_field['attr']['data-selected'] = $data->getCountry()->getCode2a();
                }
                //echo '<pre>'; print_r ($filtered_city_field); echo '</pre>'; exit;

                $form->add('cityId', ChoiceType::class, $filtered_city_field);
                $form->add('regionId', ChoiceType::class, $filtered_region_field);
                $form->add('country', CountryType::class, $filtered_country_field);
                //$event->getForm()->remove('cityId');
                //echo '<pre>'; print_r ($data->getId()); echo '</pre>'; exit;
                //echo $data; exit;
            }
        );
    }

    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        $form->add('cityId', EntityType::class, array(
            'class' => 'AppBundle:City',
            'query_builder' => function (CityRepository $city_repository) {
                return $city_repository->createQueryBuilder('c')
                    ->where('c.id != :first_country')
                    // match id of joined `CursoAcademico`
                    ->setParameter('first_country', 0);
            },
            'choice_label' => 'name',
            'label' => 'City',
            'required'    => false,
            'placeholder' => 'Choose city',
            'empty_data'  => null,
            'mapped' => true
        ));
        $form->add('regionId', EntityType::class, array(
            'class' => 'AppBundle:Region',
            'query_builder' => function (RegionRepository $region_repository) {
                return $region_repository->createQueryBuilder('r')
                    ->where('r.id != :zero')
                    // match id of joined `CursoAcademico`
                    ->setParameter('zero', 0);
            },
            'choice_label' => 'name',
            'label' => 'Region',
            'required'    => false,
            'placeholder' => 'Chooseregion',
            'empty_data'  => null,
            'mapped' => true
        ));
        // Note that the data is not yet hydrated into the entity.
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'default_cities' => [],
        ));
        /*$resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Customer',
        ));*/
    }
}

?>
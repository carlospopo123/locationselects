<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomerControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customers');
    }

    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customers/0');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customers/{id}');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customers/{id}/delete');
    }

}

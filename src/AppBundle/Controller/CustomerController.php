<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Form\Type\CustomerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends Controller
{
    /**
     * @Route("/", name="homepages")
     */
    public function HomeAction()
    {
        return $this->IndexAction();
    }

    /**
     * @Route("/customers", name="customers")
     */
    public function IndexAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');
        $customers = $repository->findAll();
        return $this->render('AppBundle:Customer:index.html.twig', array(
            'customers' => $customers
        ));
    }

    /**
     * @Route("/customers/0", name="create_customer")
     */
    public function CreateAction(Request $request)
    {
        //echo '<pre>'; print_r ($request->request->all()); echo '</pre>'; exit;
        $options = array();
        $country_repository = $this->getDoctrine()->getRepository('AppBundle:Country');
        $city_repository = $this->getDoctrine()->getRepository('AppBundle:City');
        $region_repository = $this->getDoctrine()->getRepository('AppBundle:Region');
        $first_country = $country_repository->findAll()[0];
        $options['default_cities'] = $first_country->getCities ();

            // just setup a fresh $task object (remove the dummy data)
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $form->getData();
            $post_data = $request->request->all();
            $country = $country_repository->findBy(
                array('code2a' => $post_data['customer']['location']['country'])
            )[0];
            $city = $city_repository->find($post_data['customer']['location']['cityId']);
            $customer->setCountry($country);
            $customer->setCity($city);
            $region = $region_repository->find($post_data['customer']['location']['regionId']);
            $customer->setRegion($region);
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            /*return $this->render('AppBundle:Customer:create.html.twig', array(
                'customer' => $customer,
            ));*/
            return $this->redirectToRoute('customer_saved');
        }
        return $this->render('AppBundle:Customer:create.html.twig', array(
            'customer_form' => $form->createView(),
        ));
    }

    /**
     * @Route("/customers/saved", name="customer_saved")
     */
    public function CreatedAction(Request $request)
    {
        return $this->render('AppBundle:Customer:customer_saved.html.twig', array(
        ));
    }

    /**
     * @Route("/customers/{id}", name="edit_customer")
     */
    public function EditAction($id, Request $request)
    {
        $country_repository = $this->getDoctrine()->getRepository('AppBundle:Country');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');
        $city_repository = $this->getDoctrine()->getRepository('AppBundle:City');
        $region_repository = $this->getDoctrine()->getRepository('AppBundle:Region');

        $customer = $repository->find($id);
        if (!empty($customer)) {
            $form = $this->createForm(CustomerType::class, $customer);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $customer = $form->getData();
                $post_data = $request->request->all();
                $country = $country_repository->findBy(
                    array('code2a' => $post_data['customer']['location']['country'])
                )[0];
                $city = $city_repository->find($post_data['customer']['location']['cityId']);
                $customer->setCountry($country);
                $customer->setCity($city);
                $region = $region_repository->find($post_data['customer']['location']['regionId']);
                $customer->setRegion($region);

                $em = $this->getDoctrine()->getManager();
                $em->persist($customer);
                $em->flush();

                /*return $this->render('AppBundle:Customer:edit.html.twig', array(
                    'customer' => $customer,
                ));*/
                return $this->redirectToRoute('customer_saved');
            }
            return $this->render('AppBundle:Customer:edit.html.twig', array(
                'customer_form' => $form->createView()
            ));
        }
    }

    /**
     * @Route("/customers/{id}/delete", name="delete_customer")
     */
    public function DeleteAction($id)
    {
        return $this->render('AppBundle:Customer:delete.html.twig', array(
            // ...
        ));
    }

}
?>
<?php
// src/AppBundle/Controller/RegionController.php

namespace AppBundle\Controller\API\v1;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegionController extends FOSRestController
{
    /**
     * @Rest\Get("/regions")
     */
    public function getRegionsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Region');
        $regions = $repository->findAll();
        //$view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        //return $view;
        return $regions;
    }

    /**
     * @Rest\Get("/regions/{code_or_id}")
     */
    public function getRegionAction($code_or_id, Request $request)
    {
        if (is_numeric($code_or_id)){
            $region = $this->getDoctrine()->getRepository('AppBundle:Region')->find($code_or_id);
        }else {
            $region = $this->getDoctrine()->getRepository('AppBundle:Region')->findBy(array(
               'code2a' =>  $code_or_id
            ))[0];
        }
        return $region;
    }

    /**
     * @Rest\Post("/regions")
     */
    public function postRegionsAction(Request $request)
    {
        $data = ['postRegionsAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

    /**
     * @Rest\Put("/regions/{userId}")
     */
    public function putRegionsByIdAction(Request $request)
    {
        $userId = $request->get('userId');
        $data = ['putRegionsByIdAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

    /**
     * @Rest\Delete("/regions/{userId}")
     */
    public function deleteRegionsByIdAction(Request $request)
    {
        $userId = $request->get('userId');
        $data = ['deleteRegionsByIdAction' => 'not implemented yet'];
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

}
?>
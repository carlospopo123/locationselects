<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Country;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Entity\User;
use Symfony\Component\Intl\Intl;

class LoadCountryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // get all country names in locale "locale"
        //$countries = array_values(Intl::getRegionBundle()->getCountryNames('de'));

        // get all country names for current locale
        //$countries = array_values(Intl::getRegionBundle()->getCountryNames());
        $countries_code2a = array_values(Intl::getRegionBundle()->getRegions());
        //print_r (array_values(Intl::getRegionBundle()->getLocales()));
        foreach ($countries_code2a as $code){
            $country = new Country();
            $country->setCode2a($code);
            $country->setName(Intl::getRegionBundle()->getCountryName($code));

            $manager->persist($country);
            $manager->flush();

            $this->addReference($code, $country);
        }
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}
?>
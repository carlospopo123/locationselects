<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Country;
use AppBundle\Entity\City;
use AppBundle\Entity\Region;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Entity\User;
use Symfony\Component\Intl\Intl;

class LoadRegionsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $regions = [
            "R_EE" => "East of England",
            "R_EM" => "East Midlands",
            "R_L" => "London",
            "R_NE" => "North East",
            "R_NW" => "North West",
            "R_SE" => "South East",
            "R_SW" => "South West",
            "R_WM" => "West Midlands"
        ];
        foreach ($regions as $key=>$region) {
            $new_region = new Region();
            $new_region->setName($region);
            if ($this->hasReference('GB')) {
                $country = $this->getReference('GB');
                $country->addRegion($new_region);
                $manager->persist($country);
                $manager->flush();
                $this->addReference($key, $new_region);
            }
        }
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}
?>